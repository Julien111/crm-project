package com.project.crm.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.project.crm.model.Customer;


public interface CustomerDao extends JpaRepository<Customer, Long> {
	
	@Query("from Customer c where lower(c.lastName) = lower(:lastName)")
	List<Customer> findByLastName(@Param("lastName")String lastName);

}
