package com.project.crm.aspect;

import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CRMLoggingAspect {
	
	//logger
	private Logger myLogger = Logger.getLogger(getClass().getName());
	
	//pointcut
	@Pointcut("execution(* com.project.crm.controller.*.*(..))")
	private void forControllerPackage() {}
	
	//idem for service and dao
	@Pointcut("execution(* com.project.crm.service.*.*(..))")
	private void forServicePackage() {}
	
	@Pointcut("execution(* com.project.crm.dao.*.*(..))")
	private void forDaoPackage() {}
	
	//for app flow
	
	@Pointcut("forControllerPackage() || forServicePackage() || forDaoPackage()")
	private void forAppFlow() {}
	
	//@before advice
	
	@Before("forAppFlow()")
	public void before(JoinPoint joinPoint) {
		//display method
		String method = joinPoint.getSignature().toShortString();
		myLogger.info("===> in @Before call " + method);
		//display the arguments
		Object[] args = joinPoint.getArgs();
		
		//loop
		
		for(Object argument : args) {
			myLogger.info("=> arguments : " + argument);
		}
		
	}
	
	//@afterReturning advice
	

	@AfterReturning(pointcut="forAppFlow()", returning="theResult")
	public void afterReturning(JoinPoint joinPoint, Object theResult) {
		//display method 
		String method = joinPoint.getSignature().toShortString();
		myLogger.info("===> in @AfterReturning call " + method);
		//display result
		myLogger.info("============> result: " + theResult);
		
	}

}
