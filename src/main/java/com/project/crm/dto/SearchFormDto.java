package com.project.crm.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class SearchFormDto {
	
	@NotBlank
	@NotNull	
	private String lastName;	

	public SearchFormDto() {
		super();		
	}

	public SearchFormDto(@NotBlank @NotNull String lastName) {
		super();
		this.lastName = lastName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "SearchFormDto [lastName=" + lastName + "]";
	}
		
}
