package com.project.crm.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CustomerDto {
	
	private Long id;
	
	@NotBlank
	@NotNull
	private String lastName;
	
	@NotBlank
	@NotNull
	private String firstName;
	
	@NotBlank	
	@NotNull
	@Email
	private String email;	

	public CustomerDto() {
		super();
	}

	public CustomerDto(@NotBlank String lastName, @NotBlank String firstName, @NotBlank String email) {
		super();
		this.lastName = lastName;
		this.firstName = firstName;
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "CustomerDto [id=" + id + ", lastName=" + lastName + ", firstName=" + firstName + ", email=" + email
				+ "]";
	}
	
}
