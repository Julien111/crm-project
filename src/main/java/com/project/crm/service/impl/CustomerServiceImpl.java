package com.project.crm.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.crm.dao.CustomerDao;
import com.project.crm.dto.CustomerDto;
import com.project.crm.model.Customer;
import com.project.crm.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService{
	
	@Autowired
	private CustomerDao customerDao;

	@Override
	public void saveCustomer(CustomerDto customerDto) {
		Customer customer = new Customer();
		customer.setFirstName(customerDto.getFirstName());
		customer.setLastName(customerDto.getLastName());
		customer.setEmail(customerDto.getEmail());
		// save
		this.customerDao.save(customer);		
	}

	@Override
	public void saveCustomer(Customer customer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<CustomerDto> listeCustomers() {
		List<CustomerDto> listCustomers = new ArrayList<>();
		List<Customer> theList = this.customerDao.findAll();

		for (Customer person : theList) {
			CustomerDto customer = new CustomerDto();
			customer.setId(person.getId());
			customer.setFirstName(person.getFirstName());
			customer.setLastName(person.getLastName());
			customer.setEmail(person.getEmail());
			listCustomers.add(customer);
		}
		return listCustomers;
	}

	@Override
	public CustomerDto getCustomerById(Long id) {
		Customer theCustomer = customerDao.findById(id).orElse(null);
		CustomerDto customerDto = new CustomerDto();

		customerDto.setId(theCustomer.getId());
		customerDto.setFirstName(theCustomer.getFirstName());
		customerDto.setLastName(theCustomer.getLastName());
		customerDto.setEmail(theCustomer.getEmail());
		return customerDto;
	}

	@Override
	public void updateCustomer(CustomerDto customerDto) {
		Customer customer = customerDao.findById(customerDto.getId()).orElse(null);		
		
		customer.setFirstName(customerDto.getFirstName());
		customer.setLastName(customerDto.getLastName());
		customer.setEmail(customerDto.getEmail());
		// save
		this.customerDao.save(customer);		
	}

	@Override
	public boolean existsById(Long id) {		
		return customerDao.existsById(id);
	}

	@Override
	public void deleteCustomerById(Long id) {
		customerDao.deleteById(id);			
	}

	@Override
	public List<CustomerDto> findByTheLastName(String lastName) {
		List<CustomerDto> results = new ArrayList<>();
		List<Customer> theList = this.customerDao.findByLastName(lastName);

		for (Customer person : theList) {
			CustomerDto customer = new CustomerDto();
			customer.setId(person.getId());
			customer.setFirstName(person.getFirstName());
			customer.setLastName(person.getLastName());
			customer.setEmail(person.getEmail());
			results.add(customer);
		}
		return results;		
	}

}
