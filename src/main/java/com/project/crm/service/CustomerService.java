package com.project.crm.service;

import java.util.List;

import com.project.crm.dto.CustomerDto;
import com.project.crm.model.Customer;


public interface CustomerService {
	
	void saveCustomer(CustomerDto customerDto);
	
	void saveCustomer(Customer customer);
	
	public List<CustomerDto> listeCustomers();
	
	public CustomerDto getCustomerById(Long id);
	
	void updateCustomer(CustomerDto customerDto);
	
	public boolean existsById(Long id);
	
	public void deleteCustomerById(Long id);	
	
	List<CustomerDto> findByTheLastName(String lastName);
	
}
