package com.project.crm.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.project.crm.dto.CustomerDto;
import com.project.crm.dto.SearchFormDto;
import com.project.crm.service.CustomerService;


@Controller
public class CustomerController {
		
	private CustomerService customerService;	
	
	public CustomerController(CustomerService customerService) {
		super();
		this.customerService = customerService;
	}

	@GetMapping("/list")
	public String allStudents(Model model) {
		List<CustomerDto> customers = customerService.listeCustomers();
		model.addAttribute("customers", customers);		
		return "customers/list-customers";
	}
	
	@GetMapping("/create-customer")
	public String create(Model model) {
		CustomerDto customerDto = new CustomerDto();
		model.addAttribute("customer", customerDto);
		return "customers/create";
	}
	
	@PostMapping("/customer/save")
	public String registration(@Valid @ModelAttribute("student") CustomerDto customerDto, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("customer", customerDto);			
			return "customers/create";
		}		
		customerService.saveCustomer(customerDto);
		return "pages/success";
	}
	
	@GetMapping("/edit/{id}")
	public String editStudent(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttrs) {
		CustomerDto customer = customerService.getCustomerById(id);

		if (!customerService.existsById(id)) {
			redirectAttrs.addAttribute("id", id).addFlashAttribute("errorId", "Customer do not exist.");
			return "redirect:/list";
		}

		CustomerDto customerDto = new CustomerDto();
		customerDto.setFirstName(customer.getFirstName());
		customerDto.setLastName(customer.getLastName());
		customerDto.setEmail(customer.getEmail());

		model.addAttribute("customer", customerDto);
		model.addAttribute("customer_id", customer.getId());
		return "customers/edit";
	}

	@PostMapping("/edit/{id}")
	public String editSubmit(@PathVariable("id") Long id,
			@Valid @ModelAttribute("studentForm") CustomerDto customerDto, BindingResult bindingResult,
			Model model, RedirectAttributes redirectAttrs) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("customer", customerDto);
			model.addAttribute("customer_id", id);
			return "customers/edit";
		}		
		CustomerDto customer = customerService.getCustomerById(id);		
		customer.setFirstName(customerDto.getFirstName());
		customer.setLastName(customerDto.getLastName());
		customer.setEmail(customerDto.getEmail());

		customerService.updateCustomer(customer);
		redirectAttrs.addAttribute("id", id).addFlashAttribute("msgSuccessEdit", "The customer updated.");
		return "redirect:/list";
	}
	
	@GetMapping("/delete/{id}")
	public String deleteCustomer(@PathVariable("id") Long id, RedirectAttributes redirectAttrs){
		if(!customerService.existsById(id)) {
			redirectAttrs.addAttribute("id", id).addFlashAttribute("errorId", "The customer do not exist.");
			return "redirect:/list";
		}
		else {
			customerService.deleteCustomerById(id);
			return "redirect:/list";
		}
	}
	
	@GetMapping("/search")
	public String searchWithLastName(Model model) {
		SearchFormDto form = new SearchFormDto();
		model.addAttribute("form", form);
		return "customers/search";
	}
	
	@PostMapping("/search/result")
	public String resultSearch(@Valid @ModelAttribute("form") SearchFormDto searchDto, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("form", searchDto);			
			return "customers/search";
		}		
		List<CustomerDto> customers = customerService.findByTheLastName(searchDto.getLastName());
		model.addAttribute("customers", customers);
		return "customers/find";
	}
}
